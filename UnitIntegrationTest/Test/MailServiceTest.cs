﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace UnitIntegrationTest.Test
{
    [TestFixture]
    class MailServiceTest
    {
        [Test]
        public void TestSendMail () {
            //Arrange
            var @event = new MailService();
            var @user = new User();
            var @mail = new Object();

            //Act
            //Assert
            Assert.DoesNotThrow(() => @event.SendMail(@user, @mail) );
        }


    }
}
