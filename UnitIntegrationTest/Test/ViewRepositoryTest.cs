﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace UnitIntegrationTest.Test
{
    [TestFixture]
    class ViewRepositoryTest
    {
        [Test]
        public void AddUserTest() {
            //Arrange
            var @event = new ViewRepository();
            var @user = new User();

            //Act
            //Assert
            Assert.DoesNotThrow(() => @event.AddUser(@user));
        }

        [Test]
        public void AddUser_Verify_count_list_is_one()
        {
            //Arrange
            var @event = new ViewRepository();
            var @user = new User();

            //Act
            @event.AddUser(@user);
            //Assert
            Assert.IsNotEmpty(@event.listUser);

        }
    }
}
