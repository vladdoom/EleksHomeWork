﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PatternRepositoryUnitOfWork.DAL;
using PatternRepositoryUnitOfWork.Models;
using PatternRepositoryUnitOfWork.MyRepository;

namespace PatternRepositoryUnitOfWork.Controllers
{
    public class HomeController : Controller
    {
        private IStudentRepository studentRepository;

        public HomeController() {
            this.studentRepository = new StudentRepository(new SchoolContext());
        }
        public ActionResult Index()
        {
            var students = from s in studentRepository.GetStudents()
                           select s;

            ViewBag.Stud = students;



            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}