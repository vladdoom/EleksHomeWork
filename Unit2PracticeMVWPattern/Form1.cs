﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unit2PracticeMVWPattern
{
    public partial class Form1 : Form
    {
        Label labelMove ;
        Label labelWinner;
        string text_X_O = "X";
        bool gamePlay = true;

        string[] players = new string [2];

 private  List<TrendMove> trendMoves = new List<TrendMove>();

        Button[,] buttons = new Button[3,3]; 

         struct TrendMove{
          public  int x;
           public int y;
            public TrendMove(int x1, int y2) {
                x = x1;
                y = y2;
            }
        }


        private void GetInitializeComponent() {
            players[0]= "Kasparov";
            players[1] = "Gasmanov";

            labelMove = GetlabelMove();
            labelMove.Text = "Kasparov";
            labelWinner = GetlabelWinner();

            TrendMove trendMove1 = new TrendMove(-1, +1);
            TrendMove trendMove2 = new TrendMove( 0, +1);
            TrendMove trendMove3 = new TrendMove(+1, +1);

            TrendMove trendMove4 = new TrendMove(-1, 0);
            TrendMove trendMove5 = new TrendMove(+1, 0);

            TrendMove trendMove6 = new TrendMove(-1, -1);
            TrendMove trendMove7 = new TrendMove( 0, -1);
            TrendMove trendMove8 = new TrendMove(+1, -1);


            trendMoves.Add(trendMove1);
            trendMoves.Add(trendMove2);
            trendMoves.Add(trendMove3);

            trendMoves.Add(trendMove4);
            trendMoves.Add(trendMove5);

            trendMoves.Add(trendMove6);
            trendMoves.Add(trendMove7);
            trendMoves.Add(trendMove8);


            buttons[0, 0] = getButton1();
            buttons[0, 1] = getButton2();
            buttons[0, 2] = getButton3();

            buttons[1, 0] = getButton4();
            buttons[1, 1] = getButton5();
            buttons[1, 2] = getButton6();

            buttons[2, 0] = getButton7();
            buttons[2, 1] = getButton8();
            buttons[2, 2] = getButton9();
            
            }


        public Form1()
        {
            InitializeComponent();

            GetInitializeComponent();
                }



        private void button1_Click(object sender, EventArgs e)
        {
            VerifyAll(0, 0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VerifyAll(0,1);
        }

        
        private void button3_Click(object sender, EventArgs e)
        {
            VerifyAll(0, 2);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            VerifyAll(1, 0);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            VerifyAll(1, 1);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            VerifyAll(1, 2);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            VerifyAll(2, 0);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            VerifyAll(2, 1);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            VerifyAll(2, 2);
        }

        private void VerifyAll(int x, int y)
        {
            if (gamePlay) { 
            if (VerifyEmpty(x, y))
            {
                buttons[x, y].Text = text_X_O;

                VerifyWinner(x, y);

                if (text_X_O.Equals("X"))
                {
                    text_X_O = "O";
                        labelMove.Text =players[0];
                }
                else
                {
                    text_X_O = "X";
                    labelMove.Text = players[1];
                    }
            }
        }
        }
        private void VerifyWinner(int x, int y)
        {
            foreach (TrendMove item in trendMoves)
            {
                var newX = x + item.x;
                var newY = y + item.y;
                if (newX > -1 && newX < 3
                  && newY > -1 && newY < 3)
                {
                    if (buttons[newX, newY].Text.Equals(text_X_O))
                    {
                        newX = newX + item.x;
                        newY = newY + item.y;
                        if (newX > -1 && newX < 3
                  && newY > -1 && newY < 3)
                        {
                            if (buttons[newX , newY ].Text.Equals(text_X_O))
                            {
                                labelWinner.Text = "Winner!!!!!" + labelMove.Text;
                                gamePlay = false;
                            }
                        }


                        newX = x - item.x;
                        newY = y - item.y;

                        if (newX > -1 && newX < 3
                  && newY > -1 && newY < 3)
                        {
                            if (buttons[newX, newY].Text.Equals(text_X_O))
                            {
                                labelWinner.Text = "Winner!!!!!"+labelMove.Text;
                                gamePlay = false;
                            }
                        }
                    }
                }
            }
        }

        private bool VerifyEmpty(int x, int y)
        {
            if (buttons[x, y].Text.Equals(""))
            {
                return true;
            }
            return false;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            gamePlay = true;
            labelWinner.Text ="";
            for (int i=0; i<3; i++) {
                for (int j = 0; j < 3; j++) {
                    buttons[i, j].Text = "";
                }
            }

        }
    }
}
